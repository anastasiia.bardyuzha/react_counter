## Task
Create component `Counter` that renders `h1` and `button`. Initially `h1` shows
"Count: 0". Each click on the `button` increments count.